class CustomError extends Error {
    constructor(message, status) {
        super(message);
        this.status = status;
    }
}

const customErrorHandler = (err, req, res, next) => {

    console.log(err);
    let custom_error = err;

    if (custom_error.name == "SyntaxError") {
        custom_error = new CustomError("Ge�ersiz Yaz�m", 400);
    }

    if (custom_error.name == "ValidationError") {
        custom_error = new CustomError(err.message, 400);
    }

    if (err.name === "CastError") {
        custom_error = new CustomError("L�tfen Ge�erli Bir ID Format� Girin!", 400);
    }

    if (custom_error.code == 11000) {
        custom_error = new CustomError("Alanlar� Tekrardan Kontrol Edin", 400);
    }

    res.status(custom_error.status || 500).json({
        success: false,
        message: custom_error.message || "Internal Server Error",
    });
};

module.exports = {CustomError, customErrorHandler};

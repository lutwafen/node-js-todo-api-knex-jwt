const express = require('express');
const app = express();
const db = require('./database/database');
const dotenv = require('dotenv');
const router = require('./Routes/Router');
const { customErrorHandler } = require('./CustomError');

dotenv.config({path : './config.env'});

app.use(express.json());
app.use('/', router);
app.use(customErrorHandler);

app.listen(5000, () => {
});

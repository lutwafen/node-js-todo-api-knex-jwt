
const express = require('express');
const router = express.Router();
const { getTodos, createTodo, getTodo, updateTodo, deleteTodo, todoState} = require('../Controllers/Todo');
const {CheckTodo} = require('../Middlewares/Todo/Todo');


// Todo Controller
router.get('/', getTodos);
router.get('/:todo_id', CheckTodo, getTodo);
router.post('/', createTodo);
router.put('/:todo_id',  CheckTodo, updateTodo);
router.delete('/:todo_id',  CheckTodo, deleteTodo);
router.patch('/:todo_id/:state', CheckTodo, todoState);
module.exports = router;
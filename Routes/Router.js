const express = require('express');
const router = express.Router();
const {register, login, logout} = require('../Controllers/User');
const {GetAccessToRoute} = require('../Middlewares/Auth/Auth');

const TodoRouter = require('./Todo');

router.use('/todo', GetAccessToRoute, TodoRouter);

// User Controller
router.post('/register', register);
router.post('/login', login);
router.get('/logout', GetAccessToRoute, logout);
module.exports = router;

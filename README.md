# About Application | Uygulama Hakkında 
NodeJS ile basit bir todo api uygulaması
A simple todo api application with NodeJS

# Bağımlılıklar | Dependencies
* bcryptjs 
* dotenv
* express 
* express-async-handler
* jsonwebtoken
* knex
* mysql

# Özet | Summary
Kullanıcıların giriş yaparak, kendilerine ait görev listesine erişebileceği, görevler oluşturup bunları güncelleyebileceği basit bir aplikasyon
A simple application where users can log in, access their task list, create tasks and update them

# Çalıştırmak İçin | To make it work
`npm install `

` nodemon server.js ` **or** `node server.js`
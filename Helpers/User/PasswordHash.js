var bcrypt = require("bcryptjs");
var CustomError = require('../../CustomError');
const PasswordHash = function(password, next){
    // bcrypt.genSaltSync(10, (err, salt) => {
    //     bcrypt.hashSync(password, salt, (err, hash) => {
    //         return hash;
    //     });
    // });


    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);

    return hash;
};


const ComparePassword = function(password, hashPass){
    return bcrypt.compareSync(password, hashPass);
}
    

module.exports = {PasswordHash, ComparePassword};
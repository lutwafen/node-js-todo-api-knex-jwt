const jwt = require('jsonwebtoken');


// Token Generate
const GenerateJWTFromUser = function(user){
    const {JWT_SECRET, JWT_EXPIRE} = process.env;

    const payload = {
        id : user.id,
        username: user.username
    };

    return jwt.sign(payload, JWT_SECRET, {expiresIn : JWT_EXPIRE});
};


const SendJWTToClient = function(user, res) {

    const { JWT_COOKIE } = process.env;
    let token = GenerateJWTFromUser(user);


    return res.status(200).cookie("access_token", token, {
        httpOnly : true,
        expires : new Date(Date.now() + parseInt(JWT_COOKIE) * 1000 * 60),
        secure :false
    }).json({
        success : true,
        access_token : token,
        data: {
            name : user.username,
        }
    });

}


const IsTokenIncluded = (req) => {
    return ( req.headers.authorization && req.headers.authorization.startsWith('Bearer:')  );
};


const GetAccessFromTokenHeader = (req) => {
    return req.headers.authorization.split(" ")[1];
};


module.exports = {
    GenerateJWTFromUser,
    IsTokenIncluded,
    GetAccessFromTokenHeader,
    SendJWTToClient
};
const db = require('../Database/database');
const asyncErrorHandler = require('express-async-handler');
const { CustomError } = require('../CustomError');

const getTodos = asyncErrorHandler(async (req,res,next) => {
    let todos = await db('todos').select("users.displayname", "todos.*").innerJoin('users', 'todos.user_id', 'users.id');
    

    //console.log(todos);
    return res.status(200).json({
        success: true,
        recordCount : todos.length,
        todos
    }); 
});

const createTodo = asyncErrorHandler( async (req, res, next) => {
    //console.log(req.body);
    const { title, description } = req.body;

    let insert_id = await db("todos").insert({
        title, description,
        user_id : req.user.id
    });

    let insert_data = await db("todos").select("users.displayname", "todos.*").where('todos.id', insert_id).innerJoin('users', 'todos.user_id', 'users.id');
    if (!insert_data) next(new CustomError("Ekleme Esnasında Bir Hata ile Karşılaşıldı!", 400));

    return res.status(200).json({
        success: true,
        insert_data,
       
    });
});

const getTodo = asyncErrorHandler(async (req, res, next) => {
    let todo_id = parseInt(req.params.todo_id);
    let select_data = await db("todos").select("users.displayname", "todos.*").where('todos.id', todo_id).innerJoin('users', 'todos.user_id', 'users.id');

    if (select_data.length == 0) return next(new CustomError("Herhangi bir Todo tanımı bulunamadı!", 400));

    return res.status(200).json({
        success: true,
        select_data
    });
});

const updateTodo = asyncErrorHandler(async (req,res,next) => {
    let todo_id = parseInt(req.params.todo_id);
   
    
    let {title, description} = req.body;

    await db("todos").where({id : todo_id}).update({title, description});
    
    let update_data = await db("todos").select("*").where('id', todo_id);
    
    return res.status(200).json({
        success :true,
        update_data
    });

});

const deleteTodo = asyncErrorHandler(async (req,res,next) => {
    let todo_id = parseInt(req.params.todo_id);
    
    await db('todos').where('id', todo_id).del();

    return res.status(200).json({
        success : true,
        message : "Silme işlemi başarılı"
    })

});

const todoState = asyncErrorHandler(async (req,res,next) => {
    let todo_id = parseInt(req.params.todo_id);
    let state = parseInt(req.params.state);
    console.log(state);
    if(!(state === 0 || state === 1)) return next(new CustomError("Lütfen geçerli bir güncelleştirme isteğinde bulunun!" ,400));


    req.todo.is_finish = state;
    
    await db("todos").where({id : todo_id}).update(req.todo);
    
    let update_data = await db("todos").select("*").where('id', todo_id);
    
    return res.status(200).json({
        success :true,
        update_data,
        message : "Todo durumu başarılı bir şekilde güncelleştirildi!"
    });

});


module.exports = {getTodos, createTodo, getTodo, updateTodo, deleteTodo, todoState};
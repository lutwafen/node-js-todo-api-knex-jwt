const db = require('../Database/database');
const {SendJWTToClient} = require('../Helpers/Auth/Token');
const {PasswordHash, ComparePassword} = require('../Helpers/User/PasswordHash'); 
const asyncErrorWrapper = require('express-async-handler');
const {CustomError} = require('../CustomError');

const register = asyncErrorWrapper(async (req,res,next) => {

    let {username, password, displayname} =  req.body;
    let hashPass = await PasswordHash(password, next);

    //res.status(200).json({success : true, pass : hashPass});

   
    const insert_id = await db("users").insert({username, password : hashPass, displayname});
    const user = {
        id : insert_id[0],
        username,
        displayname
    };

    
    SendJWTToClient(user, res);
});

const login = asyncErrorWrapper(async (req,res,next) => {


    const {username, password} = req.body;
    const user = await db.select('id', 'password', 'username', 'displayname').from('users').where({
        username
    });

    //console.log(user[0]);

    if(typeof user[0] === "undefined" || !ComparePassword(password, user[0].password)) return next(new CustomError("Lütfen giriş bilgilerinizi kontrol edin", 401));

    delete user[0].password;

    SendJWTToClient(user[0], res);
});

const logout = asyncErrorWrapper(async (req,res,next) => {
    
    return res.status(200).
    cookie({
        httpOnly: true,
        expires: new Date(Date.now()),
        secure: false // Development Env.
    })
    .json({success : true, message: 'Çıkış İşlemi Başarıyla Tamamlandı!'})
});


module.exports = {
    register,
    login,
    logout
};

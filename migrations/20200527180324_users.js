
exports.up = function(knex) {
    return knex.schema.createTable('users', function (table) {
        table.increments();
        table.string('username');
        table.string('password');
        table.string('dispName');
        table.dateTime('created_at');
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTable("users");
};

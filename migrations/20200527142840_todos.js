
exports.up = function(knex) {
    return knex.schema.createTable('todos', function (table) {
        table.increments();
        table.string('title');
        table.integer('user_id');
        table.text('description');
        table.dateTime('finished_date');
        table.boolean('is_finish').defaultTo(false);
        table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema
        .dropTable("todos");
};

const jwt = require('jsonwebtoken');
const db = require('../../Database/database');
const asyncErrorWrapper = require('express-async-handler');
const {CustomError} = require('../../CustomError');

const {IsTokenIncluded, GetAccessFromTokenHeader} = require('../../Helpers/Auth/Token');


const GetAccessToRoute = (req, res, next) => {
    const {JWT_SECRET} = process.env;
    if(!IsTokenIncluded(req)) return next(new CustomError("Token Bilgisi Hatalı!", 401));

    const access_token = GetAccessFromTokenHeader(req);

    console.log(access_token);

    jwt.verify(access_token, JWT_SECRET, (err, decoded) => {
        if(err) return next(new CustomError("Giriş Bilgisine Rastlanamadı!", 401));

        next();
    });


    //next();


};



module.exports = {
    GetAccessToRoute
}
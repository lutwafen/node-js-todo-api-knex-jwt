const db = require('../../Database/database');
const asyncErrorWrapper = require('express-async-handler');
const {CustomError} = require('../../CustomError');


const CheckTodo = asyncErrorWrapper(async (req,res,next) => {
    let todo_id = parseInt(req.params.todo_id);
    if(todo_id < 1 || isNaN(todo_id)) return next(new CustomError("Lütfen Bir Todo ID giriniz", 400));

    let find = await db('todos').where('id', todo_id);
    if(typeof find[0] === "undefined") return next(new CustomError("Böyle bir Todo bulunamadı!", 400));

    req.todo = find[0];

    next();
});


module.exports = {
    CheckTodo
};
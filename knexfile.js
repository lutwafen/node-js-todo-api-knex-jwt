// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database : 'knex_todoapp'
    }
  },

  staging: {
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database : 'knex_todoapp'
    }
  },

  production: {
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database : 'knex_todoapp'
    }
  }

};
